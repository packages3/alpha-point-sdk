import { Controller, Get } from '@nestjs/common';
import { AlphaPointSdk } from '../alpha-point-sdk/alpha-point-sdk';

@Controller()
export class AppController {
    constructor(private readonly ap: AlphaPointSdk) {
    }

    @Get('test')
    async get(): Promise<any> {
        const user = await this.ap.users.getByUsername({username: 'alexeylocalserver'});

        const apiKeys = await user.getApiKeys();

        return apiKeys;


        // return this.ap.products.getAll().then(a => a.map(i => i.toObject()));
    }
}

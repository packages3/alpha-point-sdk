import { Module } from '@nestjs/common';
import { AlphaPointSdkModule } from './alpha-point-sdk/alpha-point-sdk.module';
import { AppController } from './controllers/app.controller';

@Module({
    imports: [
        AlphaPointSdkModule.forRoot({
            debug: true,
            logging: true,
            defaultOmsId: 1,
            defaultOperatorId: 1,
            public: {
                name: 'AP_PUBLIC_GATEWAY',
                gateway: 'wss://apindaxstage.cdnhop.net/WSGateway',
                wsOptions: {},
            },
            admin: {
                name: 'AP_ADMIN_GATEWAY',
                gateway: 'wss://apindaxstage.cdnhop.net:10456/WSAdminGateway',
                wsOptions: {},
            },
            adminAuthRecipe: {
                // sessionToken: 'e51003df-39af-4ee4-9185-83590a31201d',
                username: 'alexeylocalserver',
                password: 'Aa123456',
            }
        })
    ],
    controllers: [
        AppController,
    ]
})
export class AppModule {
}

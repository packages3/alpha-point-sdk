import { AbstractCollection } from '../core/abstract-collection';
import { UserEndpoints } from './user.endpoints';
import { User } from './user';

export class UserCollection extends AbstractCollection {
    async getById(args: {userId: number, omsId?: number}): Promise<User> {
        const data = {
            OMSId: this.getOmsId(args),
            UserId: args.userId,
        };

        return this.client.adminClient.getOne(UserEndpoints.GetUserInfo, data, {type: User});
    }

    async getByUsername(args: {username: string, omsId?: number}): Promise<User> {
        const data = {
            OMSId: this.getOmsId(args),
            UserName: args.username,
        };

        return this.client.adminClient.getOne(UserEndpoints.GetUserInfo, data, {type: User});
    }

    async getByEmail(args: {email: string, omsId?: number}): Promise<User> {
        const data = {
            OMSId: this.getOmsId(args),
            Email: args.email,
        };

        return this.client.adminClient.getOne(UserEndpoints.GetUserInfo, data, {type: User});
    }

    async getBySessionToken(args: {sessionToken: string, omsId?: number}): Promise<User> {
        const data = {
            OMSId: this.getOmsId(args),
            SessionToken: args.sessionToken,
        };

        return this.client.adminClient.getOne(UserEndpoints.GetUserInfo, data, {type: User});
    }

    // async getAll(args: {omsId?: number} = {}): Promise<User[]> {
    //     return this.fetcher.fetchList({
    //         connection: this.context.publicGateway,
    //         type: User,
    //         endpoint: UserEndpoints.GetUserInfo,
    //         data: {
    //             OMSId: this.getOmsId(args),
    //         }});
    // }

    // async create(args: CreateProductOptions): Promise<{id: number}> {
    //     const result = await this.context.adminGateway.send<CreateProductResponse>(ProductEndpoints.AddProduct, {
    //         OMSId: this.getOmsId(args),
    //         Product: args.symbol,
    //         ProductFullName: args.fullName,
    //         ProductType: args.type,
    //         DecimalPlaces: args.decimals,
    //         NoFees: args.noFees,
    //     });
    //
    //     if (!result.Id || result.RejectMessage) {
    //         throw new Error(result.RejectMessage || 'Product creation was failed');
    //     }
    //
    //     return {id: result.Id};
    // }
}

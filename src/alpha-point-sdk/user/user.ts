import { AbstractEntity } from '../core/abstract-entity';
import { UserEndpoints } from './user.endpoints';
import { ApiKeyEndpoints } from './modules/ap-api-key/api-key.endpoints';
import { ApiKey } from './modules/ap-api-key/api-key';
import { EntityArray } from '../core/entity-array';

export interface AlphaPointUser {
    UserId: number;
    UserName: string;
    Email: string;
    PasswordHash: string;
    PendingEmailCode: string;
    EmailVerified: boolean;
    AccountId: number;
    DateTimeCreated: number;
    AffiliateId: number;
    RefererId: number;
    OMSId: number;
    Use2FA: boolean;
    Salt: string;
    PendingCodeTime: string;
    Locked: boolean;
    LockedTime: string;
    NumberOfFailedAttempt: number;
}

export interface UserInterface {
    omsId: number;
    userId: number;
    username: string;
    email: string;
    passwordHash: string;
    pendingEmailCode: string;
    emailVerified: boolean;
    accountId: number;
    dateTimeCreated: Date;
    affiliateId: number;
    refererId: number;
    use2FA: boolean;
    salt: string;
    pendingCodeTime: Date;
    locked: boolean;
    lockedTime: Date;
    numberOfFailedAttempt: number;
}

export class User extends AbstractEntity<AlphaPointUser, UserInterface> {
    async getApiKeys(): Promise<EntityArray<ApiKey>> {
        const data = {
            UserId: this.object.userId,
        };

        return this.context.client.adminClient.getList(ApiKeyEndpoints.GetUserAPIKeys, data, {
            type: ApiKey,
            relations: {
                user: this,
            }
        });
    }

    async getAffiliateCount(): Promise<any> {
        return undefined;
    }

    async getAccounts(): Promise<any[]> {
        return [];
    }

    async getPermissions(): Promise<string[]> {
        return this.context.adminGateway.send(UserEndpoints.GetUserPermissions, {UserId: this.object.userId})
    }

    async logoff(): Promise<void> {
        return undefined;
    }

    toAlphaPointObject(): AlphaPointUser {
        return User.transformToAlphaPointObject(this.object);
    }

    toObject(): UserInterface {
        return this.object;
    }

    assign(object: AlphaPointUser): void {
        this.object = User.transformToObject(object);
    }

    static transformToObject(object: AlphaPointUser): UserInterface {
        return {
            omsId: object.OMSId,
            userId: object.UserId,
            username: object.UserName,
            email: object.Email,
            passwordHash: object.PasswordHash,
            pendingEmailCode: object.PendingEmailCode,
            emailVerified: object.EmailVerified,
            accountId: object.AccountId,
            dateTimeCreated: new Date(object.DateTimeCreated),
            affiliateId: object.AffiliateId,
            refererId: object.RefererId,
            use2FA: object.Use2FA,
            salt: object.Salt,
            pendingCodeTime: new Date(object.PendingCodeTime),
            locked: object.Locked,
            lockedTime: new Date(object.LockedTime),
            numberOfFailedAttempt: object.NumberOfFailedAttempt,
        };
    }

    static transformToAlphaPointObject(object: UserInterface): AlphaPointUser {
        return {
            OMSId: object.omsId,
            UserId: object.userId,
            UserName: object.username,
            Email: object.email,
            PasswordHash: object.passwordHash,
            PendingEmailCode: object.pendingEmailCode,
            EmailVerified: object.emailVerified,
            AccountId: object.accountId,
            DateTimeCreated: object.dateTimeCreated.getTime(),
            AffiliateId: object.affiliateId,
            RefererId: object.refererId,
            Use2FA: object.use2FA,
            Salt: object.salt,
            PendingCodeTime: object.pendingCodeTime.toISOString(),
            Locked: object.locked,
            LockedTime: object.lockedTime.toISOString(),
            NumberOfFailedAttempt: object.numberOfFailedAttempt,
        };
    }
}

import { DynamicModule, Module, Provider } from '@nestjs/common';
import { SdkContext } from '../core/sdk-context';
import { ApAccountModule } from './modules/ap-account/ap-account.module';
import { ApApiKeyModule } from './modules/ap-api-key/ap-api-key.module';

@Module({
})
export class ApUserModule {
    static forSdk(context: SdkContext): DynamicModule {
        const providers: Provider[] = [
            // {
            //     provide: ProductCollection,
            //     useFactory: () => new ProductCollection(context),
            // },
        ];

        return {
            module: ApUserModule,
            imports: [
                ApApiKeyModule.forSdk(context),
                ApAccountModule.forSdk(context),
            ],
            providers,
            exports: providers,
        }
    }
}

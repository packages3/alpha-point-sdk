import { AbstractCollection } from '../../../core/abstract-collection';
import { ApiKey } from './api-key';
import { ApiKeyEndpoints } from './api-key.endpoints';
import { EntityArray } from '../../../core/entity-array';

export class ApiKeyCollection extends AbstractCollection {
    getAllByUserId(userId: number): Promise<EntityArray<ApiKey>> {
        const data = {
            UserId: userId
        };

        return this.client.adminClient.getList(ApiKeyEndpoints.GetUserAPIKeys, data, {type: ApiKey});
    }
}

import { AbstractEntity } from '../../../core/abstract-entity';
import { ApiKeyEndpoints } from './api-key.endpoints';
import { User } from '../../user';

export interface AlphaPointApiKey {
    APIKey: string;
    APISecret: string;
    UserId: number;
    Permissions: string[];
}

export interface ApiKeyInterface {
    key: string;
    secret: string;
    userId: number;
    permissions: string[];
}

export class ApiKey extends AbstractEntity<AlphaPointApiKey, ApiKeyInterface, {user: User}> {

    async remove(): Promise<void> {
        await this.context.client.adminClient.getResult(ApiKeyEndpoints.RemoveUserAPIKey, {
            UserId: this.object.userId,
            APIKey: this.object.key,
        });
    }

    get user(): User {
        return this.relations.user;
    }

    hasPermission(permission: string): boolean {
        return this.object.permissions.includes(permission);
    }

    assign(object: AlphaPointApiKey): void {
        this.object = ApiKey.transformToObject(object);
    }

    toObject(): ApiKeyInterface {
        return this.object;
    }

    toAlphaPointObject(): AlphaPointApiKey {
        return ApiKey.transformToAlphaPointObject(this.object);
    }

    static transformToAlphaPointObject(object: ApiKeyInterface): AlphaPointApiKey {
        return {
            APIKey: object.key,
            APISecret: object.secret,
            UserId: object.userId,
            Permissions: object.permissions,
        };
    }

    static transformToObject(object: AlphaPointApiKey): ApiKeyInterface {
        return {
            key: object.APIKey,
            secret: object.APISecret,
            userId: object.UserId,
            permissions: object.Permissions,
        };
    }
}

import { DynamicModule, Module, Provider } from '@nestjs/common';
import { SdkContext } from '../../../core/sdk-context';

@Module({
})
export class ApApiKeyModule {
    static forSdk(context: SdkContext): DynamicModule {
        const providers: Provider[] = [
            // {
            //     provide: ProductCollection,
            //     useFactory: () => new ProductCollection(context),
            // },
        ];

        return {
            module: ApApiKeyModule,
            providers,
            exports: providers,
        }
    }
}

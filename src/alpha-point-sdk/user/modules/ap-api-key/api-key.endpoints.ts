export enum ApiKeyEndpoints {
    GetUserAPIKeys = 'GetUserAPIKeys',
    AddUserAPIKey = 'AddUserAPIKey',
    RemoveUserAPIKey = 'RemoveUserAPIKey',
}

import { AbstractEntity } from '../../../core/abstract-entity';

export enum AccountType {
    Asset = 'Asset',
    Liability = 'Liability',
    ProfitLoss = 'ProfitLoss',
}

export enum AccountRiskType {
    Unknown = 'Unknown',
    Normal = 'Normal',
    NoRiskCheck = 'NoRiskCheck',
    NoTrading = 'NoTrading',
}

export enum AccountFeeProductType {
    BaseProduct = 'BaseProduct',
    SingleProduct = 'SingleProduct',
}

export interface AlphaPointAccount {
    OMSID: number;
    AccountId: number;
    AccountName: string;
    AccountHandle: string;
    FirmId: number;
    FirmName: string;
    AccountType: AccountType;
    FeeGroupID: number;
    ParentID: number;
    RiskType: AccountRiskType;
    VerificationLevel: number;
    CreditTier: number;
    FeeProductType: AccountFeeProductType;
    FeeProduct: number;
    RefererId: number;
    LoyaltyProductId: number;
    LoyaltyEnabled: boolean;
    MarginEnabled: boolean;
    LiabilityAccountId: number;
    LendingAccountId: number;
    ProfitLossAccountId: number;
}

export interface AccountInterface {
    omsId: number;
    accountId: number;
    accountName: string;
    accountHandle: string;
    firmId: number;
    firmName: string;
    accountType: AccountType;
    feeGroupId: number;
    parentId: number;
    riskType: AccountRiskType;
    verificationLevel: number;
    creditTier: number;
    feeProductType: AccountFeeProductType;
    feeProduct;
    refererId;
    loyaltyProductId;
    loyaltyEnabled: boolean;
    marginEnabled: boolean;
    liabilityAccountId: number;
    lendingAccountId: number;
    profitLossAccountId: number;
}

export class Account extends AbstractEntity<AlphaPointAccount, AccountInterface> {


    getPositions(): Promise<any[]> {
        return undefined;
    }

    getTrades(): Promise<any[]> {
        return undefined;
    }

    getTransactions(): Promise<any[]> {
        return undefined;
    }

    getDeposits(): Promise<any[]> {
        return undefined;
    }

    getWithdraws(): Promise<any[]> {
        return undefined;
    }

    toObject(): AccountInterface {
        return this.object;
    }

    toAlphaPointObject(): AlphaPointAccount {
        return Account.transformToAlphaPointObject(this.object);
    }

    assign(object: AlphaPointAccount): void {
        this.object = Account.transformToObject(object);
    }

    static transformToObject(object: AlphaPointAccount): AccountInterface {
        return {
            omsId: object.OMSID,
            accountId: object.AccountId,
            accountName: object.AccountName,
            accountHandle: object.AccountHandle,
            firmId: object.FirmId,
            firmName: object.FirmName,
            accountType: object.AccountType,
            feeGroupId: object.FeeGroupID,
            parentId: object.ParentID,
            riskType: object.RiskType,
            verificationLevel: object.VerificationLevel,
            creditTier: object.CreditTier,
            feeProductType: object.FeeProductType,
            feeProduct: object.FeeProduct,
            refererId: object.RefererId,
            loyaltyProductId: object.LoyaltyProductId,
            loyaltyEnabled: object.LoyaltyEnabled,
            marginEnabled: object.MarginEnabled,
            liabilityAccountId: object.LiabilityAccountId,
            lendingAccountId: object.LendingAccountId,
            profitLossAccountId: object.ProfitLossAccountId,
        };
    }

    static transformToAlphaPointObject(object: AccountInterface): AlphaPointAccount {
        return {
            OMSID: object.omsId,
            AccountId: object.accountId,
            AccountName: object.accountName,
            AccountHandle: object.accountHandle,
            FirmId: object.firmId,
            FirmName: object.firmName,
            AccountType: object.accountType,
            FeeGroupID: object.feeGroupId,
            ParentID: object.parentId,
            RiskType: object.riskType,
            VerificationLevel: object.verificationLevel,
            CreditTier: object.creditTier,
            FeeProductType: object.feeProductType,
            FeeProduct: object.feeProduct,
            RefererId: object.refererId,
            LoyaltyProductId: object.loyaltyProductId,
            LoyaltyEnabled: object.loyaltyEnabled,
            MarginEnabled: object.marginEnabled,
            LiabilityAccountId: object.liabilityAccountId,
            LendingAccountId: object.lendingAccountId,
            ProfitLossAccountId: object.profitLossAccountId,
        };
    }
}

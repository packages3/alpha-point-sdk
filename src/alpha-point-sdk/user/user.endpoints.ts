export enum UserEndpoints {
    // Public
    RegisterUser = 'RegisterUser',
    GetUserAccounts = 'GetUserAccounts',
    GetUserAffiliateCount = 'GetUserAffiliateCount',
    GetUserInfo = 'GetUserInfo',

    GetVerificationLevelConfigs = 'GetVerificationLevelConfigs',

    // Admin
    GetUserConfig = 'GetUserConfig',
    SetUserInfo = 'SetUserInfo',
    SetUserConfig = 'SetUserConfig',
    GetAllUnredactedUserConfigsForUser = 'GetAllUnredactedUserConfigsForUser',
    GetOMSUsers = 'GetOMSUsers',
    SetUserVerificationLevel = 'SetUserVerificationLevel',
    ForceUserLogoff = 'ForceUserLogoff',
    ResendVerificationEmail = 'ResendVerificationEmail',
    GetLoggedInUserBySessionToken = 'GetLoggedInUserBySessionToken',
    GetUserPermissions = 'GetUserPermissions',
    AddUserPermission = 'AddUserPermission',
    RevokeUserPermission = 'RevokeUserPermission',
    UnLockUser = 'UnLockUser',
    AddUserAffiliateTag = 'AddUserAffiliateTag',
}

import { DynamicModule, Module, Provider } from '@nestjs/common';
import { ApProductModule } from './product/ap-product.module';
import { AlphaPointSdkModuleOptions } from './alpha-point-sdk-options';
import { RpcConnection } from './core/connection/rpc.connection';
import { SdkContext } from './core/sdk-context';
import { AlphaPointSdk } from './alpha-point-sdk';
import { ApInstrumentModule } from './instrument/ap-instrument.module';
import { ApUserModule } from './user/ap-user.module';
import { ConnectionAuthenticator } from './core/connection/authenticator/connection.authenticator';

@Module({
})
export class AlphaPointSdkModule {
    static forRoot(options: AlphaPointSdkModuleOptions): DynamicModule {
        const {defaultOperatorId, defaultOmsId, logging, debug} = options;

        const context = new SdkContext({
            debug,
            logging,
            defaultOmsId,
            defaultOperatorId,
        });

        context
            .assignConnections({
                public: new RpcConnection({...options.public, logging, debug}, context),
                admin: new RpcConnection({...options.admin, logging, debug}, context),
            })
            .assignAuthenticator(new ConnectionAuthenticator());

        const providers: Provider[] = [
            {
                provide: options.admin.name,
                useFactory: async () => {
                    await context.adminGateway.connect();

                    if (options.adminAuthRecipe) {
                        return context.adminGateway.authenticate(options.adminAuthRecipe);
                    }

                    return context.adminGateway;
                },
            },
            {
                provide: options.public.name,
                useFactory: async () => context.publicGateway.connect()
            },
            {
                provide: SdkContext,
                useValue: context,
            },
            AlphaPointSdk,
        ];

        return {
            module: AlphaPointSdkModule,
            imports: [
                ApProductModule.forSdk(context),
                ApInstrumentModule.forSdk(context),
                ApUserModule.forSdk(context),
            ],
            providers,
            exports: providers,
        }
    }
}

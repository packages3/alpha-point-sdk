import { AuthRecipe } from './core/connection/authenticator/connection.authenticator';

export interface AlphaPointSdkModuleOptions extends AlphaPointSdkOptions {
    public: AlphaPointGatewayOptions;
    admin: AlphaPointGatewayOptions;
    adminAuthRecipe?: AuthRecipe;
}

export interface AlphaPointSdkOptions {
    debug: boolean;
    logging: boolean;
    defaultOmsId: number;
    defaultOperatorId: number;
}

export interface AlphaPointGatewayOptions {
    name: string;
    gateway: string;
    wsOptions: any;
}

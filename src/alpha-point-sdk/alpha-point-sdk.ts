import { SdkContext } from './core/sdk-context';
import { Injectable } from '@nestjs/common';
import { ProductCollection } from './product/product.collection';
import { InstrumentCollection } from './instrument/instrument.collection';
import { UserCollection } from './user/user.collection';
import { ApiKeyCollection } from './user/modules/ap-api-key/api-key.collection';

@Injectable()
export class AlphaPointSdk {
    public readonly products: ProductCollection;
    public readonly instruments: InstrumentCollection;
    public readonly users: UserCollection;
    public readonly apiKeys: ApiKeyCollection;

    constructor(private readonly sdkContext: SdkContext) {
        this.products = new ProductCollection(this.sdkContext);
        this.instruments = new InstrumentCollection(this.sdkContext);
        this.users = new UserCollection(this.sdkContext);
        this.apiKeys = new ApiKeyCollection(this.sdkContext);
    }

    get context(): SdkContext {
        return this.sdkContext;
    }
}

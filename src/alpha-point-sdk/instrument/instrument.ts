import { AbstractEntity } from '../core/abstract-entity';
import { UpdateInstrumentVerificationLvlConfig } from './args/update-instrument-verification-lvl-config';
import { InstrumentEndpoints } from './instrument.endpoints';

export enum InstrumentSessionStatus {
    Unknown = 'Unknown',
    Running = 'Running',
    Paused = 'Paused',
    Stopped = 'Stopped',
    Starting = 'Starting',
}

export enum InstrumentType {
    Unknown = 'Unknown',
    Standard = 'Standard',
}

export interface AlphaPointInstrument {
    OMSId: number;
    InstrumentId: number;
    Symbol: string;
    Product1: number;
    Product1Symbol: string;
    Product2: number;
    Product2Symbol: string;
    InstrumentType: InstrumentType;
    VenueInstrumentId: number;
    VenueId: number;
    SortIndex: number;
    SessionStatus: InstrumentSessionStatus;
    PreviousSessionStatus: InstrumentSessionStatus;
    SessionStatusDateTime: string;
    SelfTradePrevention: boolean;
    QuantityIncrement: number;
}

export interface InstrumentInterface {
    omsId: number;
    instrumentId: number;
    symbol: string;
    product1: number;
    product1Symbol: string;
    product2: number;
    product2Symbol: string;
    type: InstrumentType;
    venueInstrumentId: number;
    venueId: number;
    sortIndex: number;
    sessionStatus: InstrumentSessionStatus;
    previousSessionStatus: InstrumentSessionStatus;
    sessionStatusDateTime: Date;
    selfTradePrevention: boolean;
    quantityIncrement: number;
}

export class Instrument extends AbstractEntity<AlphaPointInstrument, InstrumentInterface> {
    assign(object: AlphaPointInstrument): void {
        this.object = Instrument.transformToObject(object);
    }

    async updateVerificationLevelConfig(args: UpdateInstrumentVerificationLvlConfig): Promise<void> {
        // await this.fetcher.fetchResult(this.context.adminGateway, InstrumentEndpoints.EditInstrumentVerificationLevelConfig, {
        //     InstrumentId: this.object.instrumentId,
        //     VerificationLevel: args.verificationLevel,
        //     DailyBuyLimit: args.dailyBuyLimit,
        //     DailySellLimit: args.dailySellLimit,
        //     MonthlyBuyLimit: args.monthlyBuyLimit,
        //     MonthlySellLimit: args.monthlySellLimit,
        // })
    }

    toObject(): InstrumentInterface {
        return this.object;
    }

    toAlphaPointObject(): AlphaPointInstrument {
        return Instrument.transformToAlphaPointObject(this.object);
    }

    static transformToObject(object: AlphaPointInstrument): InstrumentInterface {
        return {
            omsId: object.OMSId,
            instrumentId: object.InstrumentId,
            symbol: object.Symbol,
            product1: object.Product1,
            product1Symbol: object.Product1Symbol,
            product2: object.Product2,
            product2Symbol: object.Product2Symbol,
            type: object.InstrumentType,
            venueInstrumentId: object.VenueInstrumentId,
            venueId: object.VenueId,
            sortIndex: object.SortIndex,
            sessionStatus: object.SessionStatus,
            previousSessionStatus: object.PreviousSessionStatus,
            sessionStatusDateTime: new Date(object.SessionStatusDateTime),
            selfTradePrevention: object.SelfTradePrevention,
            quantityIncrement: object.QuantityIncrement,
        };
    }

    static transformToAlphaPointObject(object: InstrumentInterface): AlphaPointInstrument {
        return {
            OMSId: object.omsId,
            InstrumentId: object.instrumentId,
            Symbol: object.symbol,
            Product1: object.product1,
            Product1Symbol: object.product1Symbol,
            Product2: object.product2,
            Product2Symbol: object.product2Symbol,
            InstrumentType: object.type,
            VenueInstrumentId: object.venueInstrumentId,
            VenueId: object.venueId,
            SortIndex: object.sortIndex,
            SessionStatus: object.sessionStatus,
            PreviousSessionStatus: object.previousSessionStatus,
            SessionStatusDateTime: object.sessionStatusDateTime.toISOString(),
            SelfTradePrevention: object.selfTradePrevention,
            QuantityIncrement: object.quantityIncrement,
        };
    }
}

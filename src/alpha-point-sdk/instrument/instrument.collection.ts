import { AbstractCollection } from '../core/abstract-collection';
import { Instrument } from './instrument';
import { InstrumentEndpoints } from './instrument.endpoints';
import { EntityArray } from '../core/entity-array';

export class InstrumentCollection extends AbstractCollection {
    async getById(args: {instrumentId: number, omsId?: number}): Promise<Instrument> {
        const data = {
            OMSId: this.getOmsId(args),
            InstrumentId: args.instrumentId,
        };

        return this.client.publicClient.getOne(InstrumentEndpoints.GetInstrument, data, {type: Instrument});
    }

    async getAll(args: {omsId?: number} = {}): Promise<EntityArray<Instrument>> {
        const data = {
            OMSId: this.getOmsId(args),
        };

        return this.client.publicClient.getList(InstrumentEndpoints.GetInstruments, data, {type: Instrument});
    }

    // async create(args: CreateProductOptions): Promise<{id: number}> {
    //     const result = await this.context.adminGateway.send<CreateProductResponse>(ProductEndpoints.AddProduct, {
    //         OMSId: this.getOmsId(args),
    //         Product: args.symbol,
    //         ProductFullName: args.fullName,
    //         ProductType: args.type,
    //         DecimalPlaces: args.decimals,
    //         NoFees: args.noFees,
    //     });
    //
    //     if (!result.Id || result.RejectMessage) {
    //         throw new Error(result.RejectMessage || 'Product creation was failed');
    //     }
    //
    //     return {id: result.Id};
    // }
}

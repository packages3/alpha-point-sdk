export enum InstrumentEndpoints {
    // Public
    GetInstrument = 'GetInstrument',
    GetInstruments = 'GetInstruments',

    // Admin
    AddEditExchangeInstrument = 'AddEditExchangeInstrument',
    AddInstrument = 'AddInstrument',
    GetVenueInstruments = 'GetVenueInstruments',
    EditInstrumentVerificationLevelConfig = 'EditInstrumentVerificationLevelConfig',
    SetOMSFee = 'SetOMSFee',
}

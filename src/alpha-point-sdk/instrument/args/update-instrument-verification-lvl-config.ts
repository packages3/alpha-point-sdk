export interface UpdateInstrumentVerificationLvlConfig {
    verificationLevel: number;
    dailyBuyLimit: number;
    dailySellLimit: number;
    monthlyBuyLimit: number;
    monthlySellLimit: number;
}

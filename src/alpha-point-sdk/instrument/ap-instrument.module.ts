import { DynamicModule, Module, Provider } from '@nestjs/common';
import { SdkContext } from '../core/sdk-context';
import { ApProductModule } from '../product/ap-product.module';

@Module({
})
export class ApInstrumentModule {
    static forSdk(context: SdkContext): DynamicModule {
        const providers: Provider[] = [
            // {
            //     provide: ProductCollection,
            //     useFactory: () => new ProductCollection(context),
            // },
        ];

        return {
            module: ApProductModule,
            providers,
            exports: providers,
        }
    }
}

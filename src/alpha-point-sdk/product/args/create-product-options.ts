import { ProductType } from '../productInterface';

export interface CreateProductOptions {
    omsId: number;
    symbol: string;
    fullName: string;
    type: ProductType,
    decimals: number;
    noFees: boolean;
}


export interface CreateProductResponse {
    Id: number;
    RejectMessage: string;
}

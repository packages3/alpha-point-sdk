export enum ProductEndpoints {
    // Public
    GetProduct = 'GetProduct',
    GetProducts = 'GetProducts',

    // Admin
    AddProduct = 'AddProduct',
    EditVerificationLevelConfig = 'EditVerificationLevelConfig',
}

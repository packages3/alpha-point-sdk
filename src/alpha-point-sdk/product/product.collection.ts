import { Product } from './productInterface';
import { ProductEndpoints } from './product.endpoints';
import { CreateProductOptions, CreateProductResponse } from './args/create-product-options';
import { AbstractCollection } from '../core/abstract-collection';
import { EntityArray } from '../core/entity-array';

export class ProductCollection extends AbstractCollection {
    async getById(args: {productId: number, omsId?: number}): Promise<Product> {
        const data = {
            OMSId: this.getOmsId(args),
            ProductId: args.productId,
        };

        return this.client.publicClient.getOne(ProductEndpoints.GetProduct, data, {type: Product});
    }

    async getAll(args: {omsId?: number} = {}): Promise<EntityArray<Product>> {
        const data = {
            OMSId: this.getOmsId(args),
        };

        return this.client.publicClient.getList(ProductEndpoints.GetProducts, data, {type: Product});
    }

    async create(args: CreateProductOptions): Promise<{id: number}> {
        const result = await this.context.adminGateway.send<CreateProductResponse>(ProductEndpoints.AddProduct, {
            OMSId: this.getOmsId(args),
            Product: args.symbol,
            ProductFullName: args.fullName,
            ProductType: args.type,
            DecimalPlaces: args.decimals,
            NoFees: args.noFees,
        });

        if (!result.Id || result.RejectMessage) {
            throw new Error(result.RejectMessage || 'Product creation was failed');
        }

        return {id: result.Id};
    }
}

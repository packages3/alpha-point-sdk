import { AbstractEntity } from '../core/abstract-entity';

export enum ProductType {
    Unknown = 'Unknown',
    NationalCurrency = 'NationalCurrency',
    CryptoCurrency = 'CryptoCurrency',
    Contract = 'Contract',
}

export interface ProductInterface {
    omsId: number;
    productId: number;
    symbol: string;
    fullName: string;
    type: ProductType;
    decimals: number;
    tickSize: number;
    noFees: boolean;
    isDisabled: boolean;
}

export interface AlphaPointProduct {
    OMSId: number;
    ProductId: number
    Product: string;
    ProductFullName: string;
    ProductType: ProductType;
    DecimalPlaces: number;
    TickSize: number;
    NoFees: boolean;
    IsDisabled: boolean;
}

export class Product extends AbstractEntity<AlphaPointProduct, ProductInterface> {

    get isNationalCurrency(): boolean {
        return this.object.type === ProductType.NationalCurrency;
    }

    get isCryptoCurrency(): boolean {
        return this.object.type === ProductType.CryptoCurrency;
    }

    get isContract(): boolean {
        return this.object.type === ProductType.Contract;
    }

    assign(object: AlphaPointProduct): void {
        this.object = Product.transformToObject(object);
    }

    toObject(): ProductInterface {
        return this.object;
    }

    toAlphaPointObject(): AlphaPointProduct {
        return Product.transformToPointObject(this.object);
    }

    static transformToObject(object: AlphaPointProduct): ProductInterface {
        return {
            omsId: object.OMSId,
            productId: object.ProductId,
            fullName: object.ProductFullName,
            symbol: object.Product,
            type: object.ProductType,
            decimals: object.DecimalPlaces,
            tickSize: object.TickSize,
            noFees: object.NoFees,
            isDisabled: object.IsDisabled,
        };
    }

    static transformToPointObject(object: ProductInterface): AlphaPointProduct {
        return {
            OMSId: object.omsId,
            ProductId: object.productId,
            Product: object.symbol,
            ProductFullName: object.fullName,
            ProductType: object.type,
            DecimalPlaces: object.decimals,
            TickSize: object.tickSize,
            NoFees: object.noFees,
            IsDisabled: object.isDisabled,
        }
    }
}

import { AbstractEntity } from './abstract-entity';

export class EntityArray<Entity extends AbstractEntity<AlphaPointModel, SdkModel, any>, AlphaPointModel = any, SdkModel = any> {
    constructor(private readonly array: Array<Entity>) {
    }

    toArray(): any[] {

    }

    shift(): Entity {
        return undefined;
    }

    pop(): Entity {
        return undefined;
    }

    concat(array: Entity[]): this {
        return this;
    }
}


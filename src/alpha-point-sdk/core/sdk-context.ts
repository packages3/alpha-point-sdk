import { RpcConnection } from './connection/rpc.connection';
import { AlphaPointSdkOptions } from '../alpha-point-sdk-options';
import { Type } from '@nestjs/common';
import { AbstractCollection } from './abstract-collection';
import { AlphaPointClient } from './connection/alpha-point.client';
import { ConnectionAuthenticator } from './connection/authenticator/connection.authenticator';

export class SdkContext {
    public client: AlphaPointClient;
    public publicGateway: RpcConnection;
    public adminGateway: RpcConnection;
    public authenticator: ConnectionAuthenticator;

    constructor(public options: AlphaPointSdkOptions) {
        this.client = AlphaPointClient.createClient(this);
    }

    assignConnections(connections: {public: RpcConnection, admin: RpcConnection}): this {
        this.publicGateway = connections.public;
        this.adminGateway = connections.admin;
        this.client = AlphaPointClient.createClient(this);

        return this;
    }

    assignAuthenticator(authenticator): this {
        this.authenticator = authenticator;

        return this;
    }

    createCollection<T extends AbstractCollection>(type: Type<T>): T {
        return new type(this);
    }
}

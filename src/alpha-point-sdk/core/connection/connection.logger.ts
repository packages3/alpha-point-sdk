import { RpcConnectionOptions } from './rpc-connection-options';
import { Logger, LoggerService } from '@nestjs/common';

export class ConnectionLogger implements LoggerService {
    private readonly logger: Logger;
    constructor(private readonly options: RpcConnectionOptions) {
        this.logger = new Logger(`RPC ${options.name}`);
    }

    sent(message: string): void {
        if (!this.options.debug) {
            return;
        }
        this.log('Sent: ');
        console.log(JSON.parse(message.toString()));
    }

    accepted(message: string): void {
        if (!this.options.debug) {
            return;
        }
        this.log('Accept: ');
        console.log(JSON.parse(message.toString()));
    }

    connected(): void {
        if (!this.options.logging) {
            return;
        }
        this.log('Successfully connected.');
    }

    loggedOut(): void {
        if (!this.options.logging) {
            return;
        }
        this.warn('Logged out.');
    }

    webSocketClosed(): void {
        if (!this.options.logging) {
            return;
        }
        this.warn('Connection is closed.');
    }

    webSocketError(e: Error): void {
        if (!this.options.logging) {
            return;
        }
        this.error(e.message, e.stack);
    }

    warn(message: any, context?: string): void {
        this.logger.warn(message, context);
    }

    verbose(message: any, context?: string): void {
        this.logger.verbose(message, context);
    }

    log(message: string, context?: string): void {
        this.logger.log(message, context);
    }

    error(message: any, trace?: string, context?: string): void {
        this.logger.error(message, trace, context);
    }

    debug(message: any, context?: string): void {
        this.logger.debug(message, context);
    }
}

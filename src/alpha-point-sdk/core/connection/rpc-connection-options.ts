export interface RpcConnectionOptions {
    name: string;

    gateway: string;
    wsOptions: object;

    debug: boolean;
    logging: boolean;
}

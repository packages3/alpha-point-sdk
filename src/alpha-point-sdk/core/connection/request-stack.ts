export type RequestHandler = () => void | Promise<void>;

export class RequestStack {
    private readonly stack: Array<{resolve, reject}> = [];

    get size(): number {
        return this.stack.filter(x => x).length;
    }

    push(index: number, resolve: RequestHandler, reject: RequestHandler): this {
        this.stack[index] = {resolve, reject};

        return this;
    }

    resolve(index: number, data: any): this {
        if (this.stack[index]) {
            this.stack[index].resolve(data);
            delete this.stack[index];
        }

        return this;
    }

    reject(index: number, data: any): this {
        if (this.stack[index]) {
            this.stack[index].reject(data);
            delete this.stack[index];
        }

        return this;
    }
}

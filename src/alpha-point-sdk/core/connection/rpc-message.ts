export enum MessageType {
    Request,
    Reply,
    Subscribe,
    Event,
    Unsubscribe,
    Error,
}

export interface NativeRpcMessage {
    m: MessageType
    i: number;
    n: string;
    o: string;
}

export class RpcMessage {
    constructor(private nativeMessage: NativeRpcMessage) {
    }

    get type(): MessageType {
        return this.nativeMessage.m;
    }

    get endpoint(): string {
        return this.nativeMessage.n;
    }

    get index(): number {
        return this.nativeMessage.i;
    }

    get nativeModel(): any {
        try {
            return JSON.parse(this.nativeMessage.o);
        } catch (e) {
            console.log(this.nativeMessage.o  + ' - is not json format!');
            return this.nativeMessage.o;
        }
    }

    static create(index: number, endpoint: string, native: any, type: MessageType = MessageType.Request): string {
        const payload = JSON.stringify(native);

        return JSON.stringify({
            m: type,
            i: index,
            n: endpoint,
            o: payload,
        });
    }

    static fromJson(message: string): RpcMessage {
        return new RpcMessage(JSON.parse(message));
    }
}

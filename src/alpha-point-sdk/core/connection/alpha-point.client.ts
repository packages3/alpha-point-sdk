import { AbstractEntity } from '../abstract-entity';
import { SdkContext } from '../sdk-context';
import { Type } from '@nestjs/common';
import { RpcConnection } from './rpc.connection';
import { RpcResult } from './rpc-result';
import { EntityArray } from '../entity-array';

export interface RequestOptions<O> {
    type?: Type<O>;
    relations?: any
}

export class RpcClient {
    constructor(protected readonly connection: RpcConnection,
                protected readonly context: SdkContext) {
    }

    async getList<I, O extends AbstractEntity<any, any>>(endpoint: string, data: I, options: RequestOptions<O> = {}): Promise<EntityArray<O>> {
        const items = await this.connection.send(endpoint, data);

        if (!Array.isArray(items)) {
            throw new Error(`${endpoint} - expected array, given '${items}'.`);
        }

        if (!options || !options.type) {
            throw new Error('Type is not provided');
            // return items;
        }

        return new EntityArray(items);

        // return items.map(item =>  new options.type(this.context, item, options.relations || {}));
    }

    async getOne<I, O extends AbstractEntity<any, any, any>>(endpoint: string, data: I, options: RequestOptions<O> = {}): Promise<O | any> {
        const item = await this.connection.send(endpoint, data);

        if (Array.isArray(item)) {
            throw new Error(`${endpoint} - expected native object, given '${item}'.`);
        }

        if (!options || !options.type) {
            return item;
        }

        return new options.type(this.context, item, options.relations || {});
    }

    async getResult<I>(endpoint: string, data: I): Promise<RpcResult> {
        const result = await this.getOne(endpoint, data, {type: RpcResult});

        const resultObj = result.toObject();

        if (!resultObj.success) {
            throw new Error(resultObj.errorMessage || resultObj.detail || `${endpoint} - operation was failed.`);
        }

        return result;
    }
}

export class AlphaPointClient {
    constructor (public readonly adminClient: RpcClient,
                 public readonly publicClient: RpcClient) {
    }

    static createClient(context: SdkContext): AlphaPointClient {
        return new AlphaPointClient(new RpcClient(context.adminGateway, context), new RpcClient(context.publicGateway, context));
    }
}

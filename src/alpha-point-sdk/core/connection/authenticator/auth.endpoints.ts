export enum AuthEndpoints {
    WebAuthenticateUser = 'WebAuthenticateUser',
    Activate2FA = 'Activate2FA',
    Authenticate2FA = 'Authenticate2FA',
}

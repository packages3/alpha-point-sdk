import { AbstractEntity } from '../../../abstract-entity';

export interface AlphaPointAuthResult {
    Authenticated: boolean;
    errormsg?: string;
    SessionToken?: string;
    twoFaToken?: string;
}

export interface AuthResultInterface {
    authenticated: boolean;
    errorMessage?: string;
    sessionToken?: string;
    twoFaToken?: string;
}

export class AuthResult extends AbstractEntity<AlphaPointAuthResult, AuthResultInterface> {
    get isAuthenticated(): boolean {
        return this.object.authenticated;
    }

    get sessionToken(): string {
        return this.object.sessionToken;
    }

    assign(object: AlphaPointAuthResult): void {
        this.object = AuthResult.transformToObject(object);
    }

    toAlphaPointObject(): AlphaPointAuthResult {
        return AuthResult.transformToAlphaPointObject(this.object);
    }

    toObject(): AuthResultInterface {
        return this.object;
    }

    static transformToAlphaPointObject(object: AuthResultInterface): AlphaPointAuthResult {
        return {
            Authenticated: object.authenticated,
            errormsg: object.errorMessage,
            SessionToken: object.sessionToken,
            twoFaToken: object.twoFaToken,
        };
    }

    static transformToObject(object: AlphaPointAuthResult): AuthResultInterface {
        return {
            authenticated: object.Authenticated,
            errorMessage: object.errormsg,
            sessionToken: object.SessionToken,
            twoFaToken: object.twoFaToken,
        };
    }
}

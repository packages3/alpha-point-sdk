import { AbstractEntity } from '../../../abstract-entity';

export interface AlphaPointCredentials {
    UserName: string;
    Password: string;
}

export interface AuthRecipeCredentials {
    username: string;
    password: string;
}

export class Credentials extends AbstractEntity<AlphaPointCredentials, AuthRecipeCredentials> {
    assign(object: AlphaPointCredentials): void {
        this.object = Credentials.transformToObject(object);
    }

    toAlphaPointObject(): AlphaPointCredentials {
        return Credentials.transformToAlphaPointObject(this.object);
    }

    toObject(): AuthRecipeCredentials {
        return this.object;
    }

    static transformToAlphaPointObject(object: AuthRecipeCredentials): AlphaPointCredentials {
        return {
            UserName: object.username,
            Password: object.password,
        };
    }

    static transformToObject(object: AlphaPointCredentials): AuthRecipeCredentials {
        return {
            username: object.UserName,
            password: object.Password,
        };
    }
}

import { RpcConnection } from '../rpc.connection';
import { RpcClient } from '../alpha-point.client';
import { AuthRecipeCredentials, Credentials } from './entities/credentials';
import { AuthEndpoints } from './auth.endpoints';
import { AuthResult } from './entities/auth.result';
import { Logger } from '@nestjs/common';

export type AuthRecipe = AuthRecipeSessionToken | AuthRecipeCredentials | AuthRecipeApiKey;

export interface AuthRecipeSessionToken {
    sessionToken: string;
}

export interface AuthRecipeApiKey {
    userId: number;
    key: string;
    secret: string;
}

export enum AuthRecipeType {
    SessionToken,
    Credentials,
    ApiKey,
}

export type DetectedAuthRecipe =
      {type: AuthRecipeType.SessionToken, data: AuthRecipeSessionToken}
    | {type: AuthRecipeType.Credentials, data: AuthRecipeCredentials}
    | {type: AuthRecipeType.ApiKey, data: AuthRecipeApiKey};

export class ConnectionAuthenticator {

    async authenticate(connection: RpcConnection, recipe: AuthRecipe): Promise<string> {
        const detectedRecipe = this.detectAuthRecipe(recipe);

        switch (detectedRecipe.type) {
            case AuthRecipeType.Credentials:
                return this.authenticateByCredentials(connection, detectedRecipe.data)
        }

        return '';
    }

    async authenticateByCredentials(connection: RpcConnection, credentials: AuthRecipeCredentials): Promise<string> {
        const rpcClient = new RpcClient(connection, connection.context);

        const authResult = await rpcClient.getOne(AuthEndpoints.WebAuthenticateUser, Credentials.transformToAlphaPointObject(credentials), {
            type: AuthResult,
        });

        if (!authResult.isAuthenticated) {
            throw new Error(`Connection '${connection.name} is not authenticated`);
        }

        Logger.log(`Alpha Point connection successfully web authenticated.`, `RPC ${connection.name}`);

        return authResult.sessionToken;
    }

    detectAuthRecipe(recipe: any): DetectedAuthRecipe {
        if (recipe.sessionToken) {
            return {
                type: AuthRecipeType.SessionToken,
                data: {
                    sessionToken: recipe.sessionToken,
                },
            };
        }

        if (recipe.username && recipe.password) {
            return {
                type: AuthRecipeType.Credentials,
                data: {
                    username: recipe.username,
                    password: recipe.password,
                },
            };
        }

        if (recipe.userId && recipe.key && recipe.secret) {
            return {
                type: AuthRecipeType.ApiKey,
                data: {
                    userId: recipe.userId,
                    key: recipe.key,
                    secret: recipe.secret,
                },
            };
        }

        throw new Error(`Invalid authorization recipe - ${JSON.stringify(recipe)}.`);
    }
}

import { RpcConnectionOptions } from './rpc-connection-options';
import { RequestStack } from './request-stack';
import * as WebSocket from 'ws';
import { ConnectionLogger } from './connection.logger';
import { MessageType, RpcMessage } from './rpc-message';
import { SdkContext } from '../sdk-context';
import { AuthRecipe } from './authenticator/connection.authenticator';

export class RpcConnection {
    private INDEX_STEP = 2;
    protected requestIndex: number;
    protected logger: ConnectionLogger;
    protected stack: RequestStack;
    protected socket: WebSocket;
    protected readonly hash: string;

    constructor(private readonly options: RpcConnectionOptions,
                public readonly context: SdkContext) {
        this.stack = new RequestStack();
        this.logger = new ConnectionLogger(this.options);
        this.requestIndex = 0;
        this.hash = this.generateHash();
        console.log('CREATED');
    }

    get name(): string {
        return this.options.name;
    }

    getHash(): string {
        return this.hash;
    }

    async connect(): Promise<this> {
        return new Promise((resolve, reject) => {
            this.socket = new WebSocket(this.options.gateway, this.options.wsOptions);
            this.socket.onmessage = (event) => this.onMessageHandler(event);
            this.socket.onopen = () => this.onOpenHandler(resolve, reject);
            this.socket.onmessage = (event) => this.onMessageHandler(event);
            this.socket.onmessage = (event) => this.onMessageHandler(event);
        });
    }

    async authenticate(recipe: AuthRecipe): Promise<this> {
        await this.context.authenticator.authenticate(this, recipe);

        return this;
    }

    async send<T, D = any>(endpoint: string, data: D, type: MessageType = MessageType.Request): Promise<T> {
        this.requestIndex += this.INDEX_STEP;

        return new Promise((resolve, reject) => {
            this.logger.sent(JSON.stringify(data));

            this.socket.send(RpcMessage.create(this.requestIndex, endpoint, data, type), err => {
                if (err) {
                    reject(err);
                }

                this.stack.push(this.requestIndex, resolve, reject);
            });
        })
    }

    accept(message: RpcMessage): void {
        this.stack.resolve(message.index, message.nativeModel);
    }

    destroy(): void {

    }

    private onOpenHandler(resolve, reject) {
        this.logger.connected();
        resolve()
    }

    private onMessageHandler(event: WebSocket.MessageEvent): void {
        const jsonMessage = event.data.toString();
        this.logger.accepted(jsonMessage);

        this.accept(RpcMessage.fromJson(jsonMessage));
    }

    private onCloseHandler(event: any): void {
        this.logger.webSocketClosed();
    }

    private onErrorHandler(event: any): void {
        this.logger.webSocketError(event);
    }

    private generateHash(): string {
        return Math.random().toString(12).slice(2) +
            Math.random().toString(13).slice(2) +
            Math.random().toString(14).slice(2);
    }
}

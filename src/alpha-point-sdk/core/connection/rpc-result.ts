import { AbstractEntity } from '../abstract-entity';
import { NativeRpcMessage } from './rpc-message';

export enum ErrorCode {
    NotAuthorized = 20,
    InvalidResponse = 100,
    OperationFailed = 101,
    ServerError = 102,
    NotFound = 104,
}

export interface AlphaPointRpcResult {
    result: boolean;
    errormsg?: string;
    errorcode?: ErrorCode;
    detail?: string;
}

export interface RpcResultInterface {
    success: boolean;
    errorMessage?: string;
    errorCode?: ErrorCode;
    detail?: string;
}

export class RpcResult extends AbstractEntity<AlphaPointRpcResult, RpcResultInterface> {


    assign(object: AlphaPointRpcResult): void {
        this.object = RpcResult.transformToObject(object);
    }

    toObject(): RpcResultInterface {
        return this.object;
    }

    toAlphaPointObject(): AlphaPointRpcResult {
        return RpcResult.transformToAlphaPointObject(this.object);
    }

    static transformToObject(object: AlphaPointRpcResult): RpcResultInterface {
        return {
            success: object.result,
            errorMessage: object.errormsg,
            errorCode: object.errorcode,
            detail: object.detail,
        };
    }

    static transformToAlphaPointObject(object: RpcResultInterface): AlphaPointRpcResult {
        return {
            result: object.success,
            errormsg: object.errorMessage,
            errorcode: object.errorCode,
            detail: object.detail,
        };
    }
}

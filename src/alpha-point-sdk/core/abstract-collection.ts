import { SdkContext } from './sdk-context';
import { AlphaPointClient } from './connection/alpha-point.client';

export abstract class AbstractCollection {
    constructor(protected readonly context: SdkContext) {
    }

    get client(): AlphaPointClient {
        return this.context.client;
    }

    protected getOmsId(args: {omsId?: number}): number {
        return args && args.omsId || this.context.options.defaultOmsId;
    }

    protected getOperatorId(args: {operatorId?: number}): number {
        return args && args.operatorId || this.context.options.defaultOperatorId;
    }
}

import { SdkContext } from './sdk-context';

export abstract class AbstractEntity<AlphaPointModel, SdkModel, Relations = {[key: string]: AbstractEntity<any, any, any>}> {
    protected object: SdkModel;

    constructor(protected readonly context: SdkContext,
                protected readonly alphaPointModel: AlphaPointModel,
                protected readonly relations?: Relations) {
        this.assign(this.alphaPointModel);
    }

    abstract assign(object: AlphaPointModel): void;

    abstract toObject(): SdkModel;
    abstract toAlphaPointObject(): AlphaPointModel;
}
